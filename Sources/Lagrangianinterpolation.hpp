/*
 * Lagrangianinterpolation.hpp
 *
 *  Created on: Nov 21, 2014
 *      Author: vittori
 */



#ifndef LAGRANGIANINTERPOLATION_HPP_
#define LAGRANGIANINTERPOLATION_HPP_

#include "DataApproximation.hpp"
#include "Data.hpp"
#include <vector>
#include "Polynomial.hpp"
#include "Trigpoly.hpp"

/*! \brief Class which performs a lagrangian approximation.
		*
		*  The Class Lagrangianinterpolation performs a lagrangian interpolation of a data Set and outputs the interpolating polynomial.
		*/

class Lagrangianinterpolation:public DataApproximation	{

public:
	/**
	 * The constructor Lagrangianinterpolation initializes the data points which will then be interpolated.
	 */
	Lagrangianinterpolation(Data D);

	//Destructor
	~Lagrangianinterpolation();

	/**
	 * The method interpolate gets a data set using Data.hpp and creates a polynomial using one of the interpolation methods.
	 */
	Polynomial interpolate();

	/**
	 *  The method interpolate1 is not used in this class but appears here because it is present in the abstract class.
	 */
	Trigpoly interpolate1();

	/**
	 * The method calc is used only inside interpolate, it has the role of creating combinations together with the method go.
	 */
	void calc(const std::vector<int> &v, double **A, int num, int m, int index);

	/**
	 * The method go i used only inside interpolate, it has the role of creating combinations together with the method calc.
	 */
	void go(std::vector<int> &z, std::vector<int> &w, int offset, int k, double **A, int num, int m, int index);

private:

	double **sum;
	int num;
	double *x;
	double *y;

};
#endif /* LAGRANGIANINTERPOLATION_HPP_ */
