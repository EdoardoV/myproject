/*
 * Trigpoly.cpp
 *
 *  Created on: Dec 3, 2014
 *      Author: vittori
 */




#include "Trigpoly.hpp"
#include <cmath>
#include <cassert>
#include "Data.hpp"


//Constructor
Trigpoly::Trigpoly(int n, double *v1, double *v2)
{

	order = n;
	coeff1 = new double [order];
	coeff2 = new double [order];
	for (int i=0 ; i<order ; i++)
		{
			coeff1[i]=v1[i];
			coeff2[i]=v2[i];
		}

}
//Destructor
Trigpoly::~Trigpoly()
{
	delete [] coeff1;
	delete [] coeff2;
}



void Trigpoly::PrintT(double *x, double *y, int m)
{

	std::cout<< "P(x) = ";
	if (coeff1[0]==0)
	    	{}
	else
		{
			std::cout<< coeff1[0] << "/2 ";
		}

//ai coefficients
	for (int k=1; k<order; k++)
	{
		if (coeff1[k]==0)
			{}
		else if (coeff1[k]==1)
			{
				std::cout << "+cos(" << k << "x)";
			}
		else if (coeff1[k]==-1)
			{
				std::cout << "-cos(" << k << "x) + ";
			}
		else if (coeff1[k]<0)
			{
				std::cout  << coeff1[k] << "cos(" << k << "x)";
			}
		else
		{
			std::cout << "+" << coeff1[k] << "cos(" << k << "x)";
		}
	}

	//bi coefficents
	for (int k=1; k<order; k++)
	{
		if (coeff2[k]==0)
			{}
		else if (coeff2[k]==1)
			{
				std::cout << "+sin(" << k << "x)";
			}
		else if (coeff2[k]==-1)
			{
				std::cout << "-sin(" << k << "x) + ";
			}
		else if (coeff2[k]<0)
			{
				std::cout << coeff2[k] << "sin(" << k << "x)";
			}
		else if (k==order-1)
			{
				std::cout << " + " << coeff2[k] << "sin(" << k << "x)";
			}
		else
			{
				std::cout << "+" << coeff2[k] << "sin(" << k << "x)";
			}
	}
	std::cout << "\n";

	//print in matlab
	 std::ofstream write_output("PlotTr.m");
	       assert(write_output.is_open());
	       write_output << "x = "<<x[0] <<" :1/100:" << x[m-1] << ";\n";
	       write_output << " y = ";

	if (coeff1[0]==0)
	    	{}
	else
		{
			 write_output<< coeff1[0] << "/2 ";
		}


//ai coefficients
	for (int k=1; k<order; k++)
	{
		if (coeff1[k]==0)
			{}
		else if (coeff1[k]==1)
			{
				 write_output << "+cos(" << k << "*x)";
			}
		else if (coeff1[k]==-1)
			{
				 write_output << "-cos(" << k << "*x) + ";
			}
		else if (coeff1[k]<0)
			{
				 write_output  << coeff1[k] << "*cos(" << k << "*x)";
			}
		else
			{
				 write_output << "+" << coeff1[k] << "*cos(" << k << "*x)";
			}
	}

//bi coefficents
	for (int k=1; k<order; k++)
	{
		if (coeff2[k]==0)
			{}
		else if (coeff2[k]==1)
			{
				 write_output << "+sin(" << k << "*x)";
			}
		else if (coeff2[k]==-1)
			{
				 write_output << "-sin(" << k << "*x) + ";
			}
		else if (coeff2[k]<0)
			{
				write_output << coeff2[k] << "*sin(" << k << "*x)";
			}
		else if (k==order-1)
			{
				 write_output<< " + " << coeff2[k] << "*sin(" << k << "*x)";
			}
			else
			{
				 write_output << "+" << coeff2[k] << "*sin(" << k << "*x)";
			}

	}
	write_output << "; \n";
    write_output << " xi = [";
   	for (int i=0; i<m ; i++)
		{
			write_output << x[i] << " ";
		}
	write_output << "]; \n";
	write_output << " yi = [";

	for (int i=0; i<m ; i++)
		{
			write_output << y[i] << " ";
		}
	write_output << "]; \n";
   	write_output << "figure \n";
    write_output << "plot(xi,yi,'g*',x,y) \n" ;
    write_output << "legend('Original data','Interpolating polynomial')";
    write_output.close();
}

void Trigpoly::PrintP(double *x, double *y)
{

    for (int i=0; i<order; i++)
    {
    	std::cout<< "P" << i << "(x)= ";
    	if (coeff2[i]==0)
    		{}
    	else if ( coeff2[i]==1)
			{
				std:: cout << "x ";
			}
    	else
    		{
    			std::cout << coeff2[i] << "x ";
    		}
    	if (coeff1[i]<0)
			{
				std::cout << coeff1[i] << "  ";
			}
    	else if (coeff1[i]==0)
    		{}
    	else
			{
				std::cout <<"+"<< coeff1[i] << "   ";
			}
    	std::cout<<"for x in: ["<< x[i] << ", " << x[i+1] << "] \n";
    }

	//print in matlab
   std::ofstream write_output("PlotPP.m");
   assert(write_output.is_open());
   write_output << "figure \n";
   write_output << "hold on";
   write_output << "\n";
   for (int i=0; i<order; i++)
   {
	   write_output << "x = " << x[i] <<" :1/100:"  << x[i+1] <<  ";\n";
	   write_output<< "P" << i << "= ";
	   if (coeff2[i]==0)
		{}
		else if ( coeff2[i]==1)
			{
				write_output << "x ";
			}
		else
			{
				write_output<< coeff2[i] << "*x ";
			}
		if (coeff1[i]<0)
			{
				write_output << coeff1[i] << "  ";
			}
		else if (coeff1[i]==0)
			{}
		else
			{
				write_output <<"+"<< coeff1[i] << "   ";
			}

		write_output << "; \n";
		write_output << "plot(x,P" << i << ") \n" ;
   	 }

	write_output << " xi = [";
	for (int i=0; i<order+1 ; i++)
		{
			write_output << x[i] << " ";
		}
	write_output << "]; \n";

	write_output << " yi = [";
	for (int i=0; i<order+1 ; i++)
		{
			write_output << y[i] << " ";
		}
			write_output << "]; \n";
	write_output << "plot(xi,yi,'g*') \n" ;
	write_output<< "hold off  \n";
	write_output.close();
}


double Trigpoly::evaluateT (double x)
{
	double sum = coeff1[0]/2;
	for (int k=1; k<=order; k++)
		{
			double p= coeff1[k]*cos(x*k);
			sum = sum+p;
		}
	for (int k=1; k<=order; k++)
		{
			double p= coeff2[k]*sin(x*k);
			sum = sum+p;
		}
	return sum;
}

double Trigpoly::evaluateP (double x, Data D)
{
	double sum = 0;
	for (int i=0; i<order; i++)
	{
		if ( (x>=D.Getelementx(i)) && (x<=D.Getelementx(i+1)))
		{
			sum = coeff2[i]*x+coeff1[i];
		}

	}
	return sum;
}
