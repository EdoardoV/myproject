/*
 * Leaststest.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: vittori
 */


#include <iostream>
#include "Leastsquare.hpp"
#include"Polynomial.hpp"
#include"Data.hpp"
#include <cmath>

//After calling the program, you need to either get data points from a file,
//or simply input them in the terminal.
//There are several files with data points in build/opt/sources.
//You will also be asked to specify the order of the interpolating polynomial.

int main (int argc, char* argv[])
{

    Data D;
   	Leastsquare L(D);
    Polynomial P=L.interpolate();

    return 0;
}
