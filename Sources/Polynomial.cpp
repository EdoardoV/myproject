/*
 * Polynomial.cpp
 *
 *  Created on: Nov 14, 2014
 *      Author: vittori
 */


#include "Polynomial.hpp"
#include <cmath>
#include <cassert>
#include "Data.hpp"

//emtpy constructor
Polynomial::Polynomial(int n)
: order(n),
 poly(new double [n])
 {
     for (int i=0 ; i<n ; i++) poly[i]=1;
 }

//main constructor, insert value starting from lowest order
Polynomial::Polynomial(int n, double v[])
: order(n),
  poly(new double [n])
{
    for (int i=0 ; i<n ; i++) poly[i]=v[i];
}


//Destructor
Polynomial::~Polynomial()
{
	delete [] poly;
}

//Set method
void Polynomial:: Setcoefficient(int pow, double coeff)
{
	poly[pow] = coeff;
}

void Polynomial::Print(double *x, double *y, int m)
{
	int n = order;
	std::cout<< "P(x) = ";
    for (int i=0; i<n; i++)
		{
		 if (i==0)
			 {
				 if (poly[i] == 1)
					 {
						 std::cout<< "x^" <<(n-i-1);
					 }
				 else if (poly[i] == -1)
					 {
							std::cout<< "-" << "x^" <<(n-i-1);
					 }
				 else if (poly[i] == 0)
				 	 {}
				 else
					 {
						 std::cout<< poly[i] << "x^" <<(n-i-1);

					 }
				 }
		 else if (i==n-1)
			  {
				 if (poly[i]>0)
					 {
					 std::cout <<"+" << poly[i];
					 }
				 else if (poly[i]<0)
					 {
						 std::cout << poly[i];
					 }
				 else if(poly[i]==0)
				 	 {}
			  }
		 else if (poly[i]>0)
			 {
				 if (poly[i] == 1)
					 {
							std::cout<< "+" << "x^" <<(n-i-1);
					 }
				 else
					 {
						   std::cout<< "+" << poly[i] << "x^" <<(n-i-1);
					 }
			 }
		 else if (poly[i]<0)
			 {
				 if (poly[i] == -1)
				 	 {
						std::cout<< "-" << "x^" <<(n-i-1);
					 }
				 else
					 {
						  std::cout<< poly[i] << "x^" <<(n-i-1);
					 }
			 }
		 else if (poly[i] == 0)
				 {}

		}
	std::cout << "\n";

  //plot in matlab
    std::ofstream write_output("PlotPoly.m");
    assert(write_output.is_open());

    write_output << "x = "<<x[0] <<" :1/100:" << x[m-1] << ";\n";
    write_output << " y = ";
	for (int i=0; i<n; i++)
			{
			 if (i==0)
			 {
				 if (poly[i] == 1)
					 {
						 write_output<< "*power(x," <<(n-i-1) << ")";
					 }
				 else if (poly[i] == -1)
					 {
						 write_output<< "-" << "*power(x," <<(n-i-1)<< ")";
					 }
				 else if (poly[i] == 0)
					 {}
				 else
					 {
						 write_output<< poly[i] << "*power(x," <<(n-i-1)<< ")";
					 }
			 }
			 else if (i==n-1)
				  {
					 if (poly[i]>0)
						 {
							write_output<<"+" << poly[i];
						 }
					 else if (poly[i]<0)
						 {
							write_output << poly[i];
						 }
					 else if(poly[i]==0)
					 {}
				  }
			 else if (poly[i]>0)
				 {
					 if (poly[i] == 1)
						 {
							 write_output<< "+" << "*power(x," <<(n-i-1)<< ")";
						 }
					 else
						 {
							 write_output<< "+" << poly[i] << "*power(x," <<(n-i-1)<< ")";
						 }
				 }
			 else if (poly[i]<0)
				 {
					 if (poly[i] == -1)
						 {
							 write_output<< "-" << "*power(x," <<(n-i-1)<< ")";
						 }
					 else
						 {
							write_output<< poly[i] << "*power(x," <<(n-i-1)<< ")";
						 }
				 }
			 else if (poly[i] == 0)
					 {}

			}
	write_output << "; \n";

	write_output << " xi = [";
	for (int i=0; i<m ; i++)
		{
			write_output << x[i] << " ";
		}
	write_output << "]; \n";

	write_output << " yi = [";
	for (int i=0; i<m ; i++)
		{
			write_output << y[i] << " ";
		}
	write_output << "]; \n";

	write_output << "figure \n";
	write_output << "plot(xi,yi,'g*',x,y) \n" ;
	write_output << "legend('Original data','Interpolating polynomial') \n";
    write_output.close();
}

double Polynomial::evaluatePol (double x)
{

	double sum=0;
    for (int i=0; i<order; i++)
    {
    	double p=poly[i]*pow(x,order-i-1);
    	sum = sum+p;
    }
    return sum;
}



