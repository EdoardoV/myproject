/*
 * Piecewiseinterpolation.hpp
 *
 *  Created on: Nov 24, 2014
 *      Author: vittori
 */



#ifndef PIECEWISEINTERPOLATION_HPP_
#define PIECEWISEINTERPOLATION_HPP_

#include "DataApproximation.hpp"
#include "Data.hpp"
#include <vector>
#include "Trigpoly.hpp"
#include "Polynomial.hpp"

/*! \brief Class which performs a piecewise interpolation.
			 *
			 *  The Class Piecewiseinterpolation performs a piecewise interpolation of a data set and outputs the interpolating piecewise polynomial.
			 */

class Piecewiseinterpolation:public DataApproximation {

public:
	/**
	 * The constructor Piecewiseinterpolation initializes the data points which will then be interpolated.
	 */
	Piecewiseinterpolation(const Data D);

	//Destructor
	~Piecewiseinterpolation();

	/**
	 * The method interpolate creates a Piecewise Polynomial interpolation of the data set.
	 */
	Trigpoly interpolate1();

	/**
	 *  The method interpolate is not used in this class but appears here because it is present in the abstract class.
	 */
	Polynomial interpolate();

private:
	// coefficents of constant term
	double *cst;
	// coefficents of linear term
	double *lin;
	double *x;
	double *y;
	int num;

};

#endif /* PIECEWISEINTERPOLATION_HPP_ */
