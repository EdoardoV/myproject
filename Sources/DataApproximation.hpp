/*
 * DataApproximation.hpp
 *
 *  Created on: Nov 21, 2014
 *      Author: vittori
 */



#ifndef DATAAPPROXIMATION_HPP_
#define DATAAPPROXIMATION_HPP_

#include <iostream>
#include "Data.hpp"
#include "Polynomial.hpp"
#include <vector>
#include "Trigpoly.hpp"

/*! \brief Abstract class.
	 *
	 *  The Class DataApproximation is the abstract class from which the interpolation methods derive.
	 */

class DataApproximation {

public:

	//Constructor
	DataApproximation() {}

	// Destructor
	virtual ~DataApproximation() {}

	/**
	 * 	This is the method that will be overridden by the classes Leastsquare and Lagrangianinterpolation to complete a polynomial interpolation.
	 */
	virtual Polynomial interpolate()=0;

	/**
	 * 	This is the method that will be overridden by the classes fourier and Piecewiseinterpolation to complete a trigonometric or piecewise interpolation.
	 */
	virtual Trigpoly interpolate1()=0;


};
#endif /* DATAAPPROXIMATION_HPP_ */
