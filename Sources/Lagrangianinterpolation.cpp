/*
 * Lagrangianinterpolation.cpp
 *
 *  Created on: Nov 21, 2014
 *      Author: vittori
 */

#include <iostream>
#include "Lagrangianinterpolation.hpp"
#include <cmath>

//Constructor
Lagrangianinterpolation::Lagrangianinterpolation (Data D)
{
	num = D.Getlenght();

	x = new double [num];
	y = new double [num];
	for (int i = 0; i < num; i++)
		{
		  y[i] = D.Getelementy(i);
		  x[i] = D.Getelementx(i);
		}


	sum = new double *[num];
	for (int m=0; m<num; m++)
		{
			sum[m]=new double [num];
			sum[0][m]=1;
		}
}

//Destructor
Lagrangianinterpolation::~Lagrangianinterpolation ()
{
	for (int i=0; i<num; i++)
			{
				delete [] sum[i];
			}
	delete []sum;

	delete [] x;
	delete [] y;

}

Polynomial Lagrangianinterpolation::interpolate()
{
	double *coefft;
	coefft = new double [num];
	double **A;
	A = new double *[num-1];
	for (int m=0; m<num-1; m++)
		{
			A[m]=new double [num];
		}

	for (int k = 0; k<num; k++ )
	{
		double x2 = x[k];
		double coeff = 1;
		for (int j = 0; j<num; j++)
			{
				double b=0;
				if (j>k)
					{
						double	x1 = x[j];
						A[j-1][k]=x1;
						b = x2-x1;
						coeff = coeff*b;
					}
				else if (j<k)
					{
						double	x1 = x[j];
						A[j][k]=x1;
						b = x2-x1;
						coeff = coeff*b;
					}
				else
				{}
			}
		coefft[k] = coeff;
	}

	//declare vector of integers called number and one called combination
	std::vector<int> number;
	std::vector<int> combination;

	for (int i = 0; i < num-1; ++i)
			  //  sets the last entry of vector number as 1+i, so it creates a vector of size n with entries from 0 to 1
			  {
				  number.push_back(i);
			  }
	int index = 1;
	for (int k = 1; k < num; k++)
		{
			for (int m= 0; m<num; m++)
				{
					go(combination, number, 0, k, A, num, m, index);
				}
			index = index +1;
		}
	for (int i =0; i<num; i++)
		{
			for (int j = 0 ; j<num; j++)
				{
					sum[i][j]= pow(-1,i)*sum[i][j];
				}
		}
	double* v;
	v = new double [num];
	for (int i =0; i< num; i++)
		{
			double q =0;
			for (int j =0; j< num; j++)
			{
				q=q+1/coefft[j]*sum[i][j]*y[j];
			}
			v[i] = q;
		}

	Polynomial P(num,v);
	P.Print(x,y, num);

	for (int i=0; i<num-1;i++)
		{
			delete [] A[i];
		}
	delete []A;
	delete []v;
	delete []coefft;

	return P;
}


void Lagrangianinterpolation::calc(const std::vector<int> &combination, double **A, int num, int m, int index)
{
	double a=1;
	for (int j = 0; (unsigned)j <combination.size(); j++)
		{
			int b = combination[j];
			a=a*A[b][m];
		}
	if (a==0)
 	 	 {
			sum[index][m] = 0;
 	 	 }
	sum[index][m] = sum[index][m]+a;
}

void Lagrangianinterpolation::go(std::vector<int> &combination, std::vector<int> &number, int offset, int k, double **A, int num, int m, int index)
{
   if (k == 0)
  	  {
	  	  calc(combination, A, num, m, index);
		  return;
  	  }
	  // if f is not zero it enters the loop that continues until k is 0.
   for (int i = offset; (unsigned)i <= number.size() - k; ++i)
	   {
		   //  sets the last entry of vector combination as number[i]
		  combination.push_back(number[i]);
		  go(combination, number, i+1, k-1, A, num, m, index);
		   //delete last element
		  combination.pop_back();
		}
}


Trigpoly Lagrangianinterpolation::interpolate1()
{}




