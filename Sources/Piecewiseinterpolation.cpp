/*
 * Piecewiseinterpolation.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: vittori
 */

#include <iostream>
#include "Piecewiseinterpolation.hpp"
#include <cmath>
#include "Data.hpp"

//Constructor
Piecewiseinterpolation::Piecewiseinterpolation(const Data D)
{
	num = D.Getlenght();

	x = new double [num];
	y = new double [num];
	for (int i = 0; i < num; i++)
		{
			y[i] = D.Getelementy(i);
			x[i] = D.Getelementx(i);
		}

	cst = new double [num-1];
	lin = new double [num-1];
}

//Destructor
Piecewiseinterpolation::~Piecewiseinterpolation()
{
delete [] cst;
delete [] lin;
delete [] x;
delete [] y;
}


Trigpoly Piecewiseinterpolation::interpolate1()
{
	double sub[num-1];

	for (int i = 0; i < num-1; i++)
		{
			sub[i]= (y[i+1]-y[i])/(x[i+1]-x[i]);
			cst[i] = y[i] - x[i]*sub[i];
			lin[i] = sub[i];
		}

	Trigpoly P(num-1, cst, lin);
	P.PrintP(x,y);
	return P;

}

Polynomial Piecewiseinterpolation::interpolate()
{}
