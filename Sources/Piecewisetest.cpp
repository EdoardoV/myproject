/*
 * Piecewisetest.cpp
 *
 *  Created on: Nov 25, 2014
 *      Author: vittori
 */


#include <iostream>
#include "Piecewiseinterpolation.hpp"
#include"Data.hpp"
#include "cmath"

//After calling the program, you need to either get data points from a file,
//or simply input them in the terminal.
//There are several files with data points in build/opt/sources.

int main (int argc, char* argv[])
{

	//input data set
	Data D;
	Piecewiseinterpolation PI(D);

	//interpolate the data set
	Trigpoly PP=PI.interpolate1();

    //evaluate the polynomial in the interpolation points to show correctness
	for (int i=0; i<D.Getlenght(); i++)
	   {
	      double x =  PP.evaluateP(D.Getelementx(i), D);
	      std::cout<<"At x=" << D.Getelementx(i)<< " the interpolating polynomial is: " << x << "\n";
	      if (fabs(D.Getelementy(i)-x)>0.0001)
	         {
	          	std::cout<< "there is an error in the interpolation \n";
	          }
	     }
return 0;
}
