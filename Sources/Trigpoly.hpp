/*
 * Trigpoly.hpp
 *
 *  Created on: Dec 3, 2014
 *      Author: vittori
 */



#ifndef TRIGPOLY_HPP_
#define TRIGPOLY_HPP_


#include <iostream>
#include "Data.hpp"

/*! \brief Class which defines trigonometric and piecewise polynomials.
	 *
	 *  By inserting the order of the polynomials and the coefficients, I am able to print out the polynomial and evaluate it's value at a specific point x.
	 */

class Trigpoly {

public:

	/**
	 * The constructor Trigpoly creates a trigonometric or piecewise polynomial with given coefficients.
	 */
	Trigpoly(int n, double *coeff1, double *coeff2);

	//Destructor
	 ~Trigpoly();

	/**
	 * The method PrintT prints the resulting trigonometric polynomial on the screen and it exports it to a file which can then be opened by matlab. The file contains the code to plot the polynomial plus the data points.
	 */
	void PrintT(double *x, double *y, int m);

	/**
	 *  The method PrintP prints the resulting piecewise polynomial on the screen and it exports it to a file which can then be opened by matlab. The file contains the matlab code to plot the polynomial plus the data points.
	 */

	void PrintP(double *x, double *y);

	/**
	 *  The method 	evaluateT is used to evaluate the interpolating trigonometric polynomial at a specific point x.
	 */
	double	evaluateT (double x);

	/**
	 * The method  evaluateP is used to evaluate the interpolating piecewise polynomial at a specific point x.
	 */
	double  evaluateP (double x, Data D);

private:
	// Private data members
	int order;
	double *coeff1;
	double *coeff2;
};

#endif /* TRIGPOLY_HPP_ */
