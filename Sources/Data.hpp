/*
 * Data.hpp
 *
 *  Created on: Nov 16, 2014
 *      Author: EdoardoVittori
 */


#ifndef SOURCES_DATA_HPP_
#define SOURCES_DATA_HPP_

#include <cassert>
#include <iostream>
#include <fstream>
#include <string>

/*! \brief Class to input data.
	 *
	 *  The Class Data will input data pairs either from a file or from the command screen and saves the x-coordinates and the y-coordinates in two different arrays.
	 */

class Data {

public:

	/**
	 * The constructor Data inputs data pairs either from a file or from user input.
	*/	Data(void);

	/**
	 * Copy constructor.
	 */
	Data(const Data & otherData);

	// Destructor
	~Data();

	/**
	 * The method Getelementx is used to get the ith x element from the data set.
	 */
	double Getelementx(int i) const;

	/**
	 * The method Getelementy is used to get the ith y element from the data set.
	 */
	double Getelementy(int i) const;

	/**
	 * The method Getpair is used to get the ith pair.
	 */
	void Getpair( double *v, int i);

	/**
	 * The method inputdata is used to input data pairs manually.
	 */
	void inputdata();


	/**
	 * The method readdata is used to read data sets from a file.
	 */
	void readdata(std::string filename);


	/**
	 * The method Setelement changes the value of the ith pair.
	 */
	void Setelement(int i, const double x,const double y);


	/**
	 * The method Getlenght is used to get the number of pairs in the data set.
	 */
	int Getlenght() const;


private:
	int d;
	double *xc;
	double *yc;

};

#endif /* SOURCES_DATA_HPP_ */
