//  fourier.cpp
//  
//
//  Created by Edoardo Vittori on 27/11/14.
//
//


#include <iostream>
#include "fourier.hpp"
#include <math.h>
#include <cassert>
#include <stdlib.h>

//Constructor
fourier::fourier (Data D)
{
	num = D.Getlenght();

	x = new double [num];
	y = new double [num];
	for (int i = 0; i < num; i++)
		{
		  y[i] = D.Getelementy(i);
		  x[i] = D.Getelementx(i);
		}
	double d = cos(0);
	std::cout << "initial " << d << "\n";
}

//Destructor
fourier::~fourier ()
{
	delete [] x;
	delete [] y;
}

Trigpoly fourier::interpolate1()
{
	assertdata(x);
	double h= 2*M_PI/(num);
	int M;
	if (num % 2 == 0)
		{
			M = num/2;
		}
	else
		{
			M = (num-1)/2;

		}

	double *sumc;
	double *sums;

	sumc = new double [M+1];
	sums = new double [M+1];


	//Fourier series

	for (int k=0; k<=M; k++)
		{

			double sumcos = 0;
			double sumsin = 0;

			for (int j=0; j<num; j++)
				{
					double angle = k*j*h;
					double cos1=y[j]*cos(angle);
					double sin1=y[j]*sin(angle);
					if (fabs(cos1)<pow(10, -14))
						{
							cos1 = 0;
						}
					if (fabs(sin1)<pow(10, -14) )
						{
							sin1=0;
						}
					sumcos=sumcos+cos1;
					sumsin=sumsin+sin1;
				}
			if (num%2 == 0 && k == M)
				{
					sums[k]= 0;
					sumc[k]= sumcos/(num);
				}
			else
				{
					sumc[k]=2*sumcos/(num);
					sums[k]=2*sumsin/(num);
				}
			}

	Trigpoly P(M+1, sumc, sums);
	P.PrintT(x,y, num);

	delete []sumc;
	delete []sums;

	return P;
}

void fourier::assertdata(double *v)
{
	for (int i=0; i<num; i++)
		{
			double coor = (2*M_PI*i)/num;
			double diff = v[i]-coor;
			if (fabs(diff)>0.001)
				{
					std::cout<<"error, coordinates not correct \n";
					exit (EXIT_FAILURE);
				}

	}
}

Polynomial fourier::interpolate()
{}

