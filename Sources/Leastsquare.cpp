/*
 * Leastsquare.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: vittori
 */


#include <iostream>
#include "Leastsquare.hpp"
#include <cmath>
#include <cassert>

//Constructor
Leastsquare::Leastsquare (Data D)
{
	//I create a new matrix where I will store the values of the individual polynomial coefficients
	num = D.Getlenght();

	x = new double [num];
	y = new double [num];
	for (int i = 0; i < num; i++)
		{
		  y[i] = D.Getelementy(i);
		  x[i] = D.Getelementx(i);
		}

}

//Destructor
Leastsquare::~Leastsquare ()
{
	delete [] x;
	delete [] y;
}

Polynomial Leastsquare::interpolate()
{
	int m;
	std:: cout << "Enter polynomial degree (must be less than the number of pairs)\n";
	std:: cin >> m;

	double **matrix;
	matrix =  new double *[m+1];

	for (int i=0; i<m+1; i++)
		{
			matrix[i]=new double [m+1];
		}

	for (int k=0; k<m+1; k++)
		{
			for (int j= 0 ; j<m+1 ; j++)
				{
					double sum=0;
					for (int i=0; i<num; i++)
						{
							double s= pow(x[i],j+k);
							sum =sum+s;
							matrix[k][j] = sum;
						}
					}
		}

	//export matrix to MATLAB to calculate the condition number
	std::ofstream write_output("CondiM.m");
	assert(write_output.is_open());

	write_output << "condmatrix = [";

	for (int k=0; k<m+1; k++)
		{
			for (int j= 0 ; j<m+1 ; j++)
				{
					write_output << matrix[k][j] << " ";
				}
			if (k==m)
			{}
			else
			{
			write_output << ";";
			}
		}
	write_output << "]; \n";
	write_output << "cond(condmatrix)";
	write_output.close();
	double *RHS;
	RHS = new double [m+1];

	//back to calculations
	for (int j= 0 ; j<m+1 ; j++)
	{
		double s= 0;
		for (int i=0; i<num ; i++)
		{
			 s=pow(x[i],j)*y[i];
				RHS[j]=RHS[j]+s;

		}

	}

	GaussPartialPivoting(matrix, RHS, m+1);

	double* v;
	v = new double [m+1];
	BackwardSolve(matrix, RHS, m+1, v);
	double invert[m+1];
	for (int i=0; i<m+1; i++)
		{
			invert[i]=v[m-i];
		}

	Polynomial P(m+1,invert);
	P.Print(x,y, num);

	for (int i=0; i<m+1;i++)
		{
			delete [] matrix[i];
		}
	delete []matrix;
	delete []v;
	delete []RHS;
	return P;

}

//this performs a guassian elimination
void Leastsquare::GaussPartialPivoting(double** A, double* b, const int n)
{
	for (int k = 0; k < n; ++k)
	{
		Pivot(A, b, k, n);

		for (int i = k + 1; i < n; ++i)
			{
				double m = A[i][k] / A[k][k];
				for (int j = k + 1; j < n; ++j)
				{
				A[i][j] -= m * A[k][j];
				}
				b[i] -= m * b[k];
			}
		for (int i = k + 1; i < n; ++i)
			{
			A[i][k] = 0;
			}
	}
}

//this pivots and puts on top the column with the highest first term
void Leastsquare::Pivot(double** A, double* b, const int k, const int n)
{
	double max = std::fabs(A[k][k]);
	int idx_max = k;

	for (int i = k + 1; i < n; ++i)
		{
			double ev = std::fabs(A[i][k]);
			if (ev > max)
			{
				max = ev;
				idx_max = i;
			}
		}

	// Swap values in rhs b
	double t = b[k];
	b[k] = b[idx_max];
	b[idx_max] = t;

	// Swap rows in matrix A
	double* p = A[k];
	A[k] = A[idx_max];
	A[idx_max] = p;
}

//backward solves the matrix
void Leastsquare::BackwardSolve(double** A, double* b, const int n, double* x)
{
	for (int i = n - 1; i >= 0; --i)
	{
		x[i] = b[i];
		for (int j = i + 1; j < n; ++j)
			{
				x[i] -= A[i][j] * x[j];
			}
		x[i] /= A[i][i];
	}
}

Trigpoly Leastsquare::interpolate1()
{}



