/*
 * Data.cpp
 *
 *  Created on: Nov 16, 2014
 *      Author: EdoardoVittori
 */

#include "Data.hpp"
#include <cassert>
#include <iostream>
#include <fstream>
#include <cstdlib>


Data::Data(void)
{
	// I create a variable filename which will contain characters and not numbers
	std::string filename;
	std:: cout << "Write filename or write input if you want to manually input data\n";
	// I give a value to the filename (which is the name of the text file)
	std:: cin >> filename;
	// I create an object of ifstream which is called read_file and it will open the file of which I have written the name
	std::ifstream read_file(filename.c_str());
	//std::ifstream read_file(filename);
	if (read_file.fail())
		{

			int b = 0;
			std::cout << "File does not exist, press 1 to exit or 2 to input data manually \n";
			std::cin >> b;
			if (b == 1)
				{
					exit(1);
				}
			else if (b==2)
				{
					inputdata();
				}

		}
	else
		{
			readdata(filename);
		}
	read_file.close();

}

//copy constructor
Data::Data(const Data & otherData)
{
	d = otherData.Getlenght();
	xc=new double [d];
	yc=new double [d];

	for (int i=0; i<d; i++)
	    {
	        xc[i] = otherData.Getelementx(i);
	        yc[i] = otherData.Getelementy(i);
	    }
}

// this function reads the data from file and creates the arrays x and y, I input the name of the file I need to open (it is all done automatically)
void Data::readdata (std::string filename)
{
	//I create the same object as above, which has been forgotten as now we are in a new function
	std::ifstream read_file(filename.c_str());
	//If it doesn't open the file correctly, I'll get an error message.
	assert(read_file.is_open());
	int i = 0;
	double c = 0;

	while (!read_file.eof())
		{
			i = i+1;
			read_file >> c;
		}
	d = i/2;
	std::cout << "there are " << d<< " pairs\n";

	xc=new double [d];
	yc=new double [d];

	// rewind file to start reading from beginning
	read_file.clear();
	read_file.seekg(std::ios::beg);

	for (int i =0; i<d; i++)
		{
			read_file >> xc[i] >> yc[i];
		}
	read_file.close();
}



//inputdata function, this lets you input data manually
void Data::inputdata()
{
	std::cout <<" How many pairs are you inputting ? \n";
	std:: cin >> d ;
	xc=new double [d];
	yc=new double [d];
	std::cout <<" Write the x components, individually followed by enter \n";
	for (int i=0; i<d; i++)
		{
			std::cin >> xc[i];
		}
	std::cout <<" Write the y components, individually followed by enter \n";
	for (int i=0; i<d; i++)
		{
			std::cin >> yc[i];
		}
}


//destructor
Data::~Data()
{
	delete [] xc;
	delete [] yc;

}

//accessor methods
// to obtain the x coordinate (remember we are counting from 0 so x[3] = element 4)
double Data::Getelementx(int i) const
{
	return xc[i];
}

//to obtain the y coordinate
double Data::Getelementy(int i) const
{
	return yc[i];
}


void Data::Getpair( double *v,int i)
{
	v[0] = xc[i];
	v[1] = yc[i];
}


int Data::Getlenght() const
{
	return d;
}

// Set method
void Data::Setelement(int i, const double a, const double b)
{
	xc[i] = a;
	yc[i] = b;
}




