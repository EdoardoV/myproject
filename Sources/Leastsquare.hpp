/*
 * Leastsquare.hpp
 *
 *  Created on: Nov 26, 2014
 *      Author: vittori
 */



#ifndef LEASTSQUARE_HPP_
#define LEASTSQUARE_HPP_

#include "DataApproximation.hpp"
#include "Data.hpp"
#include "Polynomial.hpp"
#include "Trigpoly.hpp"

/*! \brief Class which performs a least squares interpolation.
		 *
		 *  The Class Leastsquare performs a least squares interpolation of a data set: by inputting not only the data set but also the order of the desired polynomial, you obtain the best fit polynomial.
		 */

class Leastsquare:public DataApproximation {

public:
	/**
	 * The constructor Leastsquare initializes the data points which will then be approximated.
	 */
	Leastsquare(Data D);

	//Destructor
	~Leastsquare();

	/**
	 *  The method interpolate gets a data set using Data and creates a polynomial using the least squares method. It also exports the matrix solved with the Gaussian elimination method to Matlab. This gives the possibility to calculate the condition number of the matrix.
	 */
	Polynomial interpolate();

	/**
	 *  The method interpolate1 is not used in this class but appears here because it is present in the abstract class.
	 */
	Trigpoly interpolate1();

	/**
	 * The method GaussPartialPivoting performs a guassian elemination.
	 */
	void GaussPartialPivoting(double** A, double* b, const int n);

	/**
	 *  The method Pivot, pivots and puts on top the row with the highest first term.
	 */
	void Pivot(double** A, double* b, const int k, const int n);

	/**
	 *  The method backward solve, solves the system.
	 */
	void BackwardSolve(double** A, double* b, const int n, double* x);

private:

	int num;
	double *x;
	double *y;

};

#endif /* LEASTSQUARE_HPP_ */
