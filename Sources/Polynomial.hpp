/*
 * Polynomial.hpp
 *
 *  Created on: Nov 24, 2014
 *      Author: vittori
 */

#ifndef _Polynomial_hpp
#define _Polynomial_hpp

#include <iostream>
#include "Data.hpp"

/*! \brief Class which defines polynomials.
	 *
	 *  By inserting the order of the polynomials and the coefficients, I am able to print out the polynomial and evaluate it's value at a specific point x.
	 */

class Polynomial {

public:
	/**
	 * The constructor Polynomial creates a polynomial of size n with all the coefficients equal to one.
	 */
	Polynomial(int n);

	/**
	 * The constructor Polynomial creates a polynomial with given coefficients starting from the highest order.
	 */
	Polynomial(int n, double poly[]);

	// Destructor
	 ~Polynomial();

	/**
	 * The method Setcoefficient is used change the value of the ith coefficient.
	 */
	 void Setcoefficient(int pow, double coeff);

	/**
	 * The method Print prints the resulting polynomial on the screen and it exports it to a file which can then be opened by matlab. The file contains the matlab code to plot the polynomial plus the data points.
	 */
	 void Print(double *x, double *y, int m);

	/**
	 * The method evaluatePol is used to evaluate the interpolating polynomial at a specific point x.
	 */
	 double 	evaluatePol (double x);


private:
	// Private data members
	int order;
	double *poly;

};

#endif /* POLYNOMIAL_HPP_ */
