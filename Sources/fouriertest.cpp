/*
 * fouriertest.cpp
 *
 *  Created on: Dec 2, 2014
 *      Author: vittori
 */



#include <iostream>
#include "fourier.hpp"
#include"Polynomial.hpp"
#include"Data.hpp"
#include <cmath>

//After calling the program, you need to either get data points from a file,
//or simply input them in the terminal.
//There are several files with data points in build/opt/sources.
//OutputF.txt, OutputF1.txt and OutputF2.txt are the ones specifically for this type of interpolation.

int main (int argc, char* argv[])
{
//input data set
    Data D;
    fourier F(D);

//interpolate the data set
    Trigpoly T = F.interpolate1();

//evaluate the polynomial in the interpolation points to show correctness
    for (int i=0; i<D.Getlenght(); i++)
    {
        double x = T.evaluateT(D.Getelementx(i));
        std::cout<<" At x=" << D.Getelementx(i)<< " the interpolating polynomial is: " << x << "\n";
        if (fabs(D.Getelementy(i)-x)>0.001)
        {
        	std::cout<< "there is an error in the interpolation \n";
        }
    }

    return 0;
}

