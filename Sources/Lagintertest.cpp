//
//  Lagintertest.cpp
//  
//
//  Created by Edoardo Vittori on 23/11/14.
//
//

#include <iostream>
#include "Lagrangianinterpolation.hpp"
#include"Polynomial.hpp"
#include"Data.hpp"
#include <cmath>

//After calling the program, you need to either get data points from a file,
//or simply input them in the terminal.
//There are several files with data points in build/opt/sources.

int main (int argc, char* argv[])
{
//input data set
    Data D;
    Lagrangianinterpolation L(D);

//interpolate the data set
    Polynomial P= L.interpolate();

//evaluate the polynomial in the interpolation points to show correctness
      for (int i=0; i<D.Getlenght(); i++)
      {
          double x = P.evaluatePol(D.Getelementx(i));
          std::cout<<" At x=" << D.Getelementx(i)<< " the interpolating polynomial is: " << x << "\n";
          if (fabs(D.Getelementy(i)-x)>0.001)
         	   {
        	 	   std::cout<< "there is an error in the interpolation \n";
           	  }
       }

   return 0;
}
