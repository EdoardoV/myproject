//
//  fourier.hpp
//  
//
//  Created by Edoardo Vittori on 27/11/14.
//
//


#ifndef _fourier_hpp
#define _fourier_hpp

#include "DataApproximation.hpp"
#include "Data.hpp"
#include "Trigpoly.hpp"

/*! \brief Class which performs a trigonometric interpolation.
	 *
	 *  The Class fourier performs a discrete Fourier Series of a periodic data Set which satisfies f(0)=f(2pi) and outputs the interpolating trigonometric polynomial.
	 */

class fourier:public DataApproximation {

public:

	/**
	 * The constructor fourier initializes the points which will then be interpolated.
	 */
	fourier(Data D);

	//Destructor
	~fourier();

	/**
	 *  The method interpolate1 gets a Data set using Data.hpp and creates a polynomial using discrete Fourier Series.
	 */
	Trigpoly interpolate1();

	/**
	 *  The method interpolate is not used in this class but appears here because it is present in the abstract class.
	 */
	Polynomial interpolate ();


	/**
	 * The method assertdata makes sure that the data points are correctly distanced for a Fourier approximation.
	 */
	void assertdata(double *v);

private:
	int num;
	double *x;
	double *y;


};
#endif
